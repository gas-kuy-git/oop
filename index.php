<?php
require_once('Mobil.php');
require_once('Animal.php');
require_once('Frog.php');
require_once('Ape.php');

$mobil = new Mobil("Toyota");
echo "Nama merk : ".$mobil->name ."<br>";
echo "Banyak Roda : ".$mobil->roda ."<br>";
echo "Bahan bakar : ".$mobil->bahanbakar ."<br>";
echo "<br>";

$sheep = new Animal("Shaun");
echo "Nama Binatang : ".$sheep->name ."<br>";
echo "Banyak Kaki : ".$sheep->legs ."<br>";
echo "Berdarah dingin : ".$sheep->cool_blooded ."<br>";
echo "<br>";

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"
echo "Nama Binatang : ".$kodok->name ."<br>";
echo "Banyak Kaki : ".$kodok->legs ."<br>";
echo "Berdarah dingin : ".$kodok->cool_blooded ."<br>";
echo "Jump : ".$kodok->jump ."<br>";
echo "<br>";


$sungokong = new Ape("kera sakti");
$sungokong->yell(); // ambil funsinya
echo "Nama Binatang : ".$sungokong->name ."<br>";
echo "Banyak Kaki : ".$sungokong->legs ."<br>";
echo "Berdarah dingin : ".$sungokong->cool_blooded ."<br>";
echo "Yell : " .$sungokong->yell ."<br>";
echo "<br>";



?>